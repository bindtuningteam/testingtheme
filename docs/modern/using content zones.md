The content zones built into the theme allow you to add custom html to the header and footer through the following steps:


1. Click **Edit** on the top right corner of the page content;

	<p class="alert alert-success">Alternatively, if you’re on a library page, you can open up your browser’s inspector by pressing F12 and on the console and typing <strong>“btEditMode()”</strong>.</p>

2. This will put the page on edit mode. The header and footer will reveal editable content areas. 	
3. Hover the section you wish to edit and click the **Edit** button ✏️;
4. Add your custom HTML and click **Save** and then **Publish**;

	<p class="alert alert-success">Alternatively, if you’re on a library page, you can open up your browser’s inspector by pressing F12 and on the console and typing <strong>btEditModeSave()</strong>.</p>

![contentzones.png](../images/contentzones.png)

You’re done! ✅
