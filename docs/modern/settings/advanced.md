Settings for Administrators

<img src="../../../images/bindtuning-settings-advanced.png" alt="BindTuning Settings Advanced"	title="BindTuning Settings Advanced" width="300" />

---

#### Settings source
Allows you to change where your settings are retrieved from

---

#### Import/Export
Allows you to import settings from a specific site or JSON file and export to several or into a JSON file.

---