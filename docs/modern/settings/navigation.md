Set up and fine tune your navigation.

<img src="../../../images/bindtuning-settings-navigation.png" alt="BindTuning Settings General"	title="BindTuning Settings General" width="300" />

---

#### Navigation Source
- <b>Dynamic</b>: uses the default SharePoint navigation you have set up [default];
- <b>Current</b>: uses SharePoint current navigation;
- <b>Global</b>: uses SharePoint global navigation;
- <b>Term set</b>: Allows you to use a custom term set as your site navigation;
- <b>Legacy</b>: uses the old navigation provider (BindMENU options unavailable);

---

#### Use managed navigation
Uses managed navigation if set

---

#### Hide recent items
Hides the recent items menu item

---

#### Hide Recycle bin
Hides the recycle bin menu item (available for team sites only)

---

#### Term Set ID
Takes in the ID of your term set, found in SharePoint’s term store management;

---

#### Enable accessibility features
Activate features for keyboard use of the navigation. <a href="https://support.bindtuning.com/hc/en-us/articles/360030533692-BindTuning-Navigation-Accessibility">Find out more here.</a>

---

#### Sub-menu style
- <b>Default</b>: normal sub-menu hierarchy, presenting one level at a time;
- <b>Mega Menu</b>: displays the entire hierarchy of menu items in one big sub-menu;

---

#### Type of animation
- <b>Linear</b>: fade-in animaton occurs at a constant pace;
- <b>Ease</b>: fade-in animation starts slowly, then accelerates and then slows down again;
- <b>Ease-in</b>: fade in animation starts slowly then accelerates;
- <b>Ease-out</b>: fade in animation starts quickly and slows down near the end;

---

#### Sub-menu fade in speed
- <b>Fast</b>: sub-menu takes 200ms to fade in;
- <b>Normal</b>: sub-menu takes 300ms to fade in;
- <b>Slow</b>: sub-menu takes 400ms to fade in;

---

#### Sub-menu fades in from the
- <b>Top</b>;
- <b>Right</b>;
- <b>Bottom</b>;
- <b>Left</b>;

---

#### Maximum sub-menu width
Set in PX, you can use the slider or the input to right of the setting label, where no restrictions apply;

---

#### Minimum sub-menu width
Set in PX, you can use the slider or the input to right of the setting label, where no restrictions apply;

---

#### Maximum sub-menu width
Set in PX, you can use the slider or the input to right of the setting label, where no restrictions apply;

---

#### Max number of columns <small>(mega menu only)</small>
Set the maximum number of columns, creating a line break at the end of each row;

---

#### Max number of horizontal menu items
Sets the maximum number of menu items, hiding the remaining ones in a sub-menu;