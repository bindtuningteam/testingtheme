/**
Global Vars
 */

var spanIDs = [];
var lastScrollTop = 0;
var $window = jQuery(window);
var $header = "";
var $footer = "";
var $sideNavWrapper = "";
var $sideNav = "";
var $size = {};

var path = {
    partials: "theme/partial/"
};

var loadding = '<div id="bt-loading" class="bt-loading block-ui-active" style="z-index: 99;position: absolute; height: 100%; width:100%">' +
    '<div class="bt-loading-wrapper">' +
    '<div class="page-spinner-bar">' +
    '<div class="bounce1"></div>' +
    '<div class="bounce2"></div>' +
    '<div class="bounce3"></div>' +
    '</div>' +
    '</div>' +
    '</div>';

var flags = {
    "header": false,
    "footer": false,
    "extrafiles": false,
    "mobile": false,
    "openSideBar": true //The SideBar is open on *False*
}

// Initialize
INIT();

/**
 * Methods
 */
function INIT() {
    jQuery(loadding).appendTo("body");
    detectmob();
    jQuery('<link rel="stylesheet" type="text/css" href="//bindtuning.com/site2018/assets/icons/bt-icons.min.css?v1.0.2" />').appendTo('head')
    jQuery('<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500">').appendTo('head')

    jQuery(document).on("ready", function() {

        if (window.location.href.toLowerCase() === 'https://bindtuning-office-365-themes.readthedocs.io/en/latest/setup/upload%20the%20demo%20content/') {
            window.location.href = 'https://support.bindtuning.com/hc/en-us/articles/360001046646-How-to-apply-demo-content-on-classic-SharePoint-sites';
        }

        jQuery.fn.hasScrollBar = function() {
            return this.get(0).scrollHeight > this.outerHeight();
        };

        function getScrollBarWidth() {
            var $outer = jQuery('<div>').css({
                    visibility: 'hidden',
                    width: 100,
                    overflow: 'scroll'
                }).appendTo('body'),
                widthWithScroll = $('<div>').css({
                    width: '100%'
                }).appendTo($outer).outerWidth();
            $outer.remove();
            return 100 - widthWithScroll;
        };
        var scrollWidth = getScrollBarWidth();
        if (jQuery('body').hasScrollBar() && scrollWidth > 0) {
            jQuery('header.home').css('left', '-' + scrollWidth + 'px');
            jQuery('header.home').css('padding-left', scrollWidth + 'px');
        }
    });

    // menuCollapse();

    // Verify if all is injected
    var checkExist = setInterval(function() {
        if (flags.header && flags.footer && flags.extrafiles) {
            cleanHTTPflags();

            // Button Action on click in opensideBar (BT-OpenSideBar -> RTD )
            jQuery("i.BT-OpenSideBar").on("click", function() {
                if (flags.openSideBar) {
                    jQuery('nav.wy-nav-side').css("display", "block");
                    setTimeout(function() {
                        jQuery("nav.wy-nav-side").css("left", "0px");
                        flags.openSideBar = false;
                    }, 250);
                } else {
                    jQuery("nav.wy-nav-side").css("left", "-300px");
                    flags.openSideBar = true;
                }
            });

            // Allow hover on BindTuning Navbar
            jQuery("li.BT-menu-base.has-children").hover(function() {
                jQuery(this).addClass('open');
                jQuery(this).children('ul').removeClass('is-hidden');
            }).mouseleave(function() {
                jQuery(this).removeClass('open');
                jQuery(this).children('ul').addClass('is-hidden');
            });

            //Init Styles of SideBar (Read the Docs)
            initSideBarStyles();

            // Hide BT-Loading
            jQuery("#bt-loading").css("display", "none");

            clearInterval(checkExist);
        }
    }, 500);

    jQuery.get("/en/latest/" + path.partials + "extra_files.html", function(data) {
        jQuery("head").append(data);

        setTimeout(function() {
            flags.extrafiles = true;
        }, 250);
    });

    // Read Header -> BindTuning Site (header.html)
    jQuery.get("/en/latest/" + path.partials + "header.html", function(data) {
        jQuery(".wy-grid-for-nav").append(data)

        setTimeout(function() {
            flags.header = true;
        }, 250);
    });

    // Read Footer -> BindTuning Site (footer.html)     
    jQuery.get("/en/latest/" + path.partials + "footer.html", function(data) {
        jQuery(".wy-grid-for-nav").append(data);

        setTimeout(function() {
            flags.footer = true;
        }, 250);
    });

}

// Add class to RTD SideBar Menu to change to a Collapsable Menu
function menuCollapse() {

    //Remove element on Introduction <a></a> on BindTuning Account Menu
    jQuery('li.toctree-l1 ul.subnav li ul').eq(0).remove();

    // Vars
    var elementBtn = "span.caption-text"
    var elementUl = "li.toctree-l1 ul.subnav"
    var spansMenu = jQuery(elementBtn);
    var ulMenu = jQuery(elementUl);

    //Fill all Buttons on Menu 
    for (var x = 0; x < spansMenu.length; x++) {
        var spanID = jQuery(elementBtn)[x].textContent.toLowerCase().replace(" ", "_");
        jQuery(elementBtn).eq(x).attr("data-target", "#" + spanID);
        jQuery(elementBtn).eq(x).attr("data-toggle", "collapse");
        jQuery(elementBtn).eq(x).attr("aria-expanded", "true");
        jQuery(elementBtn).eq(x).attr("aria-controls", spanID);
        jQuery(elementBtn).eq(x).addClass("text-bold");

        spanIDs.push(spanID);

        // At the end Fill Respectives subMenus
        if (x === (spansMenu.length - 1) && spanIDs.length === spansMenu.length) {

            for (var z = 0; z < ulMenu.length; z++) {
                jQuery(elementUl).eq(z).addClass("collapse in");
                jQuery(this).eq(z).css('display', 'inherit');

                jQuery(elementUl).eq(z).addClass("bt-collapse");
                jQuery(elementUl).eq(z).attr("id", spanIDs[z]);
            }

        }
    }

}

//Init Styles to Device
function initSideBarStyles() {
    $header = jQuery('header.home.hero');
    $footer = jQuery('footer.bg-ocean');
    $sideNavWrapper = jQuery('div.wy-menu.wy-menu-vertical');
    $sideNav = jQuery('nav.wy-nav-side');

    $size["window"] = $window.height();
    $size["header"] = $header.height();
    $size["footer"] = $footer.height();
    $size["footerToTop"] = $footer.offset().top;
    $size["topBottomDif"] = parseInt($sideNav.css("top").replace("px", "")) - $header.height();

    // Hide Elements 
    // SideBar
    jQuery('nav.wy-nav-side').css("display", "none");
    // RTD Theme
    jQuery('.wy-nav-content footer').css("color", "#fff").children().not('div.rst-footer-buttons').hide()

    detectmob();

    // Choose Styles if has a mobile or a desktop device
    if (flags.mobile) {
        initMobileSideBar();
    } else {
        initDesktopSideBar();
    }

}

//Init SideBar for Desktop Device
function initDesktopSideBar() {
    jQuery('.BT-OpenSideBar').css("display", "none");
    jQuery('nav.wy-nav-side').css("display", "block");
    jQuery('nav.wy-nav-side').css("left", "40px");
    jQuery('body').scrollTop(0);
    scrollTopCSS();

    jQuery('nav.wy-nav-side').removeClass('BT-MobileMenu');

    var calcSideNavHeight = $size.window - ($size.header + $size.footer + $size.topBottomDif);

    // $sideNavWrapper && $sideNav
}


//Init SideBar for Mobile Device
function initMobileSideBar() {

    //Show Button to Mobile
    jQuery('.BT-OpenSideBar').css("display", "inherit");
    jQuery("nav.wy-nav-side").css("left", "-330px")
    flags.openSideBar = true;


    //Add class to navbar
    jQuery('nav.wy-nav-side').addClass('BT-MobileMenu');

    var calcSideNavHeight = $size.window - $size.header;

    // $sideNavWrapper && $sideNav
    jQuery('div.wy-menu.wy-menu-vertical, nav.wy-nav-side').css("height", calcSideNavHeight + "px");

}

function cleanHTTPflags() {
    flags.header = false;
    flags.footer = false;
    flags.extrafiles = false;
}

function detectmob() {
    if (window.innerWidth < 769) {
        flags.mobile = true;
    } else {
        flags.mobile = false;
    }
}
//Styles to apply on Scroll Down
function scrollDownCSS() {
    $sideNav.css({
        "position": "absolute",
        "z-index": "1",
        "top": $(document).height() - ($(window).height() + (flags.mobile ? 70 : 110)) + "px",
    });
}
//Styles to apply on Scroll Top
function scrollTopCSS() {

    var topDistance;
    if (flags.mobile) {
        topDistance = "70px";
    } else {
        topDistance = "110px";
    }

    $sideNav.css({
        "position": "fixed",
        "z-index": "1",
        "top": topDistance,
    });
}

/***
 * Listners
 */

// Hide content of collapse
jQuery('.bt-collapse').on('hidden.bs.collapse', function() {
    jQuery(this).css('display', 'none');
});

// Show content of collapse
jQuery('.bt-collapse').on('show.bs.collapse', function() {
    jQuery(this).css('display', 'inherit');
});

// On Resize the page "reload" styles
jQuery(window).on('resize', function() {
    initSideBarStyles();
});
// Scroll event for sticky sidenav
jQuery(window).on('scroll', function() {

    if (flags.mobile === false) {
        var calcFixedPosition = $sideNavWrapper.offset().top + $sideNavWrapper.height();
        var bodyScroll = jQuery(this).scrollTop();

        if (bodyScroll > lastScrollTop) {
            // downscroll code
            if ($footer.offset().top <= (calcFixedPosition + 90)) {
                scrollDownCSS();
            }

        } else {
            // upscroll code
            if ((jQuery(this).scrollTop() + jQuery(window).height()) < ($footer.offset().top + 30)) {
                scrollTopCSS();
            }

        }
        lastScrollTop = bodyScroll;
    }
});