Inside your theme's *SandboxSolution* folder you will find a .wsp file which you will be using for installing your theme. 


1. Open your SharePoint Site;
2. Click on **Settings** ⚙️ and then on **Site settings**;
3. Under "Web Designer Galleries", click on **Solutions**;
	
	**Note:** If you're not working on your root site this option will not appear. 
	
4. Click on **Upload Solution**;
5. Upload the *"yourthemename".SPO2013.wsp* file. You can find the file by opening your theme's pack and the *SandboxSolution* folder;
6. Click OK; 
7. **Click on "Activate"** to activate the theme.
	
Theme installed! ✅ 

---

### Set the master page 

Final step is to change your current master page to one of the theme's master page.

1. Open the **Settings** menu and click on **Site Settings**;  
2. Under **Look and Feel**, click on **Master page**.
	
	![changethemasterpage_1](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/changethemasterpage_1.png) 
 
3. Choose the master page you want to apply. Your theme's master pages starts by *yourthemename*;

	![changethemasterpage_3](https://bitbucket.org/bindtuningteam/sharepoint2013-docs/wiki/images/changethemasterpage_3.png) 
4. Click **Ok**. 

Master Page set! ✅